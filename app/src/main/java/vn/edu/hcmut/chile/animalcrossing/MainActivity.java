package vn.edu.hcmut.chile.animalcrossing;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    TextView txtPoint;
    CheckBox cb1,cb2,cb3;
    SeekBar sb1,sb2,sb3;
    ImageButton ibtnPlay;
    int point=5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Map();
        txtPoint.setText(point+"");
        DisEnableSeekBar();

        final CountDownTimer countDownTimer =new CountDownTimer(60000,300) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (sb1.getProgress() >= sb1.getMax()){
                    this.cancel();
                    ibtnPlay.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this, "Turtle Win", Toast.LENGTH_SHORT).show();
                    if(cb1.isChecked()){
                        point+=10;
                        Toast.makeText(MainActivity.this, "Bạn đoán chính xác", Toast.LENGTH_SHORT).show();
                    }else{
                        point-=5;
                        Toast.makeText(MainActivity.this, "Bạn đoán sai rồi ", Toast.LENGTH_SHORT).show();
                    }
                    txtPoint.setText(point+"");
                    EnableCheckbox();
                }
                if(sb2.getProgress() >= sb2.getMax()){
                    this.cancel();
                    ibtnPlay.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this, "Rabbit Win", Toast.LENGTH_SHORT).show();

                    if(cb2.isChecked()){
                        point+=10;
                        Toast.makeText(MainActivity.this, "Bạn đoán chính xác", Toast.LENGTH_SHORT).show();

                    }else{
                        point-=5;
                        Toast.makeText(MainActivity.this, "Bạn đoán sai rồi ", Toast.LENGTH_SHORT).show();

                    }
                    txtPoint.setText(point+"");
                    EnableCheckbox();
                }
                if (sb3.getProgress() >= sb3.getMax()){
                    this.cancel();
                    ibtnPlay.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this, "Frog Win", Toast.LENGTH_SHORT).show();

                    if(cb3.isChecked()){
                        point+=10;
                        Toast.makeText(MainActivity.this, "Bạn đoán chính xác", Toast.LENGTH_SHORT).show();
                    }else{
                        point-=5;
                        Toast.makeText(MainActivity.this, "Bạn đoán sai rồi ", Toast.LENGTH_SHORT).show();

                    }
                    txtPoint.setText(point+"");
                    EnableCheckbox();
                }

                int number=10;
                Random random = new Random();
                int one=random.nextInt(number);
                int two=random.nextInt(number);
                int three=random.nextInt(number);

                sb1.setProgress(sb1.getProgress() + one);
                sb2.setProgress(sb2.getProgress()+two);
                sb3.setProgress(sb3.getProgress()+three);

            }

            @Override
            public void onFinish() {

            }
        };

        ibtnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (point < 5){
                    Toast.makeText(MainActivity.this, "Bạn không đủ điểm để chơi.Vui lòng mua thêm điểm", Toast.LENGTH_SHORT).show();
                }else if(cb1.isChecked() || cb2.isChecked() || cb3.isChecked()){

                    sb1.setProgress(0);
                    sb2.setProgress(0);
                    sb3.setProgress(0);
                    ibtnPlay.setVisibility(View.INVISIBLE);
                    countDownTimer.start();
                    DisEnableCheckbox();
                }else{
                    Toast.makeText(MainActivity.this, "Vui lòng đặt cược", Toast.LENGTH_SHORT).show();
                }

            }
        });

        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    cb2.setChecked(false);
                    cb3.setChecked(false);
                }
            }
        });
        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    cb1.setChecked(false);
                    cb3.setChecked(false);
                }
            }
        });
        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    cb1.setChecked(false);
                    cb2.setChecked(false);
                }
            }
        });


    }
    private void EnableCheckbox(){
        cb1.setEnabled(true);
        cb2.setEnabled(true);
        cb3.setEnabled(true);
    }
    private void DisEnableCheckbox(){
        cb1.setEnabled(false);
        cb2.setEnabled(false);
        cb3.setEnabled(false);
    }
    private void DisEnableSeekBar(){
        sb1.setEnabled(false);
        sb2.setEnabled(false);
        sb3.setEnabled(false);
    }
    private void Map(){
        txtPoint    = (TextView) findViewById(R.id.textViewPoint);
        cb1         = (CheckBox) findViewById(R.id.checkbox1);
        cb2         = (CheckBox) findViewById(R.id.checkbox2);
        cb3         = (CheckBox) findViewById(R.id.checkbox3);
        sb1         = (SeekBar) findViewById(R.id.seeBar1);
        sb2         = (SeekBar) findViewById(R.id.seeBar2);
        sb3         = (SeekBar) findViewById(R.id.seeBar3);
        ibtnPlay    = (ImageButton) findViewById(R.id.buttonPlay);
    }
}
